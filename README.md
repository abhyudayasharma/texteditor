# TextEditor

[![Build Status](https://travis-ci.com/AbhyudayaSharma/TextEditor.svg?branch=master)](https://travis-ci.com/AbhyudayaSharma/TextEditor)

A simple text editor made using Java 11 for a university project.

## Running the app

Clone the repository and open the Maven Project in your favorite Java IDE. Compile and run.

## Documentation

See the latest documentation [here](https://abhyudayasharma.github.io/TextEditor).
